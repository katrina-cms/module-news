<?php
	class news_app_controller extends app_controller {
		
		private $page_count = 4;
		
		/**
		 * Метод формирования списка новостей
		 */
		public function index($pid, $cid = 0, $page = 0) {
		
			// получаем данные по разделу
			$data = $this->news->getVolume($pid);
			if (empty($data)) {
				return $this->page_404();
			}
			
			// передаем в шаблонизатор
			$data['cid']	= (int)$cid;
			$data['page']	= (int)$page;
			
			// доступные форматы первью изображений
			$formats = $this->_config->get('formats', 'news');
			
			// формируем список новостей
			$data['news_list'] = $this->news->getList($pid, $cid, $page, $this->page_count);
			if (!empty($data['news_list'])) {
				foreach ($data['news_list'] as $i => &$item) {
					
					// формирование URL новости и даты
					$item['date'] = strtotime($item['date']);
					$item['link'] = '/news/item/'.$item['pid'].'/'.date('d-m-Y', $item['date']).'/'.$item['id'].'/';
					$item['date'] = (int)date('d', $item['date']).' '.$this->_date->month($item['date']);
					
					// превью
					if (!empty($formats)) {
						foreach ($formats as $i => $f) {
							$img_path = $this->src_path.$f['folder'].DS.(int)$item['id'].'.jpg';
							if (file_exists($img_path)) {
								$item['img_'.$f['folder']] = '/modules/news/src/'.$f['folder'].'/'.(int)$item['id'].'.jpg';
							}
						}
					}
				}
				$data['news_list'] = $this->_tpl->render('news/index_list', $data);
			}
			
			// если это AJAX
			if (self::$is_ajax) {
				die(json_encode(array(
					'status'	=> 'ok',
					'data'		=> $data
				)));
			}
			
			// помечаем активный раздел
			self::$volume_id = (int)$data['id'];
			
			// SEO
			$this->_tpl->assign('meta_description',	$data['description']);
			$this->_tpl->assign('meta_keywords',	$data['keywords']);
			
			// добавляем title в заголовок
			self::$title[] = $data['title'];
			
			// получаем список категорий
			$data['categories_list'] = $this->news->getCategories($pid);
			if (!empty($data['categories_list'])) {
				foreach ($data['categories_list'] as $i => &$cat) {
					$cat['link'] = '/news/'.$data['id'].'/'.$cat['id'].'/';
					if ($cid == $cat['id']) {
						$cat['active'] = 'active';
					}
				}
			}
			
			// добавляем категорию "Все"
			array_unshift($data['categories_list'], array(
				'link'		=> '/news/'.$data['id'].'/',
				'title'		=> 'Все',
				'active'	=> empty($cid) ? 'active' : ''
			));
			
			// основной рендер
			$this->_tpl->render('news/index', $data, 'content');
		}
		
		/**
		 * Метод подробной страницы новости
		 */
		public function item($pid = 0, $date = '', $id = 0) {
				
			// работаем с конкретной новостью по id
			$data = $this->news->getItemActive($id);
			if (empty($data)) {
				return $this->page_404();
			}
			
			// помечаем активный раздел
			self::$volume_id = (int)$data['pid'];
			
			// доступные превью для новости
			$formats = $this->_config->get('formats', 'news');
			if (!empty($formats)) {
				foreach ($formats as $i => $f) {
					$img_path = $this->src_path.$f['folder'].DS.(int)$data['id'].'.jpg';
					if (file_exists($img_path)) {
						$data['img_'.$f['folder']] = '/modules/news/src/'.$f['folder'].'/'.(int)$data['id'].'.jpg';
					}
				}
			}
			
			// формирование даты
			$data['date'] = strtotime($data['date']);
			$data['date'] = (int)date('d', $data['date']).' '.$this->_date->month($data['date']);
			
			// текстовый контент
			if (file_exists($this->src_path.'text'.DS.$data['id'].'.txt')) {
				$data['text'] = file_get_contents($this->src_path.'text'.DS.$data['id'].'.txt');
			}
			
			// Фотогалерея (слайдер)
			$data['photogallery_block'] = $this->photogallery_controller->block($data['id'], $this->module_id);
			
			$this->_css->assign('news/item');
			$this->_tpl->render('news/item', $data, 'content');
		}

	}
?>