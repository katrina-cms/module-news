<div class="customTabsBlock">
	<ul class="customTabs btn-group toolbar">
		<li><a href="#content_inner" class="buttonL bDefault downBtn">Содержание страницы</a></li>
		<li><a href="#seo" class="buttonL bDefault">SEO</a></li>
		{if $photogallery_block}
			<li><a href="#photogallery" class="buttonL bDefault">Фотогалерея</a></li>
		{/if}
	</ul> 
</div>
<form action="/{$admin_dir}/news/item_save/" method="post" class="page_form" enctype="multipart/form-data">
	<input type="hidden" name="pid" value="{$pid}" />
	<input type="hidden" name="id" value="{$id}" />
	<input type="hidden" name="config[image_pos]" value="{$config_image_pos}" id="imagePosField" />
	
	<div class="widget fluid" id="content_inner">
		<div class="formRow">
			<div class="grid3"><label>Заголовок новости:</label></div>
			<div class="grid9"><input type="text" name="title" value="{$title}" maxlength="75" /></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Краткое описание:</label></div>
			<div class="grid9"><textarea name="note">{$note}</textarea></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Категория:</label></div>
			<div class="grid9">
				<select class="styled" name="category_id">
					<option value="0">Все</option>
					{foreach $categories_list as $category}
						<option value="{$category.id}" {if $category_id == $category.id}selected{/if}>{$category.title}</option>
					{/foreach}
				</select>
			</div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Изображение:</label></div>
			<div class="grid9">
				{if $img_100x56}
					{if $img_100x56}<a class="lightbox" href="{$img_500x300}">{/if}
						<img width="100" height="56" src="{$img_100x56}" />
					{if $img_100x56}</a>{/if}
					
					{if $img_200x120}
						<a id="roundedPreview" class="pos_{$config_image_pos}" href="#">
							<img height="56" src="{$img_200x120}" />
						</a>
					{/if}
					
					<br />
				{/if}
				<input type="file" name="preview" />
			</div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Текст страницы:</label></div>
			<div class="grid9"><textarea name="text" class="wysiwyg" rel="2:{$id}" id="text_id">{$text}</textarea></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Дата:</label></div>
			<div class="grid9"><input type="text" class="datepicker" name="date" value="{$date}" /></div>
		</div>
		<div class="formRow on_off">
			<div class="grid3"><label for="inmenuField">Показывать на сайте:</label></div>
			<div class="grid9">
				<input type="hidden" name="is_show" value="0" />
				<input type="checkbox" name="is_show" value="1" id="isShowField" {$is_show_checked} />
			</div>
		</div>
		<div class="formRow on_off">
			<div class="grid3"><label for="inmenuField">Показывать на главной странице:</label></div>
			<div class="grid9">
				<input type="hidden" name="is_show_main" value="0" />
				<input type="checkbox" name="is_show_main" value="1" id="isShowMainField" {$is_show_main_checked} />
			</div>
		</div>
		<div class="formRow">
			<input type="submit" class="buttonS bLightBlue" value="Сохранить" />
		</div>
	</div>
	<div class="widget fluid" id="seo" style="display:none;">
		<div class="formRow">
			<div class="grid3"><label>Описание страницы:</label></div>
			<div class="grid9"><input type="text" name="description" value="{$description}" /></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Ключевые слова:</label></div>
			<div class="grid9"><input type="text" name="keywords" value="{$keywords}" /></div>
		</div>
		<div class="formRow">
			<input type="submit" class="buttonS bLightBlue" value="Сохранить" />
		</div>
	</div>
	{$photogallery_block}
</form>

<div id="popupRoundedPreview" style="display:none;" title="Превью">
	<div class="formRow fluid">
		<div class="grid3"><label>Варианты:</label></div>
		<div class="grid9">
			<a class="imageView iv1 active" data-pos="left" href="#">
				<span></span>
				<img height="120" src="{$img_500x300}" />
			</a>
			<a class="imageView iv2" data-pos="center" href="#">
				<span></span>
				<img height="120" src="{$img_500x300}" />
			</a>
			<a class="imageView iv3" data-pos="right" href="#">
				<span></span>
				<img height="120" src="{$img_500x300}" />
			</a>
		</div>
	</div>
</div>