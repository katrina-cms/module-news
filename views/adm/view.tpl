<div class="customTabsBlock">
	<ul class="customTabs btn-group toolbar">
		<li><a href="#list" class="buttonL bDefault downBtn">Список новостей</a></li>
		<li><a href="#settings" class="buttonL bDefault">Настройки</a></li>
		<li><a href="#seo" class="buttonL bDefault ">SEO</a></li>
	</ul> 
</div>
<div class="widget fluid" id="list">
	<div class="formRow">
		<a class="button buttonS bLightBlue" href="/{$admin_dir}/news/add/8/">Добавить новость</a>
	</div>
	<div id="dyn">
		<div id="dynamic_wrapper" class="dataTables_wrapper" role="grid">
			<table cellpadding="0" cellspacing="0" border="0" class="dTable dataTable" id="dynamic" aria-describedby="dynamic_info">
				<thead>
					<tr>
						<td></td>
						<td></td>
						<td>Дата</td>
						<td>Заголовок новости</td>
						<td>Категория</td>
						<td></td>
						<td></td>
					</tr>
				</thead>
				<tbody>
					{foreach $news_list as $item}
						<tr>
							<td width="1%"><a class="tablectrl_small bDefault tipS deleteBtn" rel="{$item.id}" href="#" original-title="Удалить"><span data-icon="" class="iconb"></span></a></td>
							<td width="1%">
								{if $item.img_100x56}
									<a href="{$item.img_500x300}" class="lightbox">
										<img width="100" height="56" src="{$item.img_100x56}" />
									</a>
								{/if}
							</td>
							<td width="1%"><nobr>{$item.date}</nobr></td>
							<td><a href="/{$admin_dir}/news/item/{$item.pid}/{$item.id}/">{$item.title}</a></td>
							<td width="1%"><nobr>{if $item.category_title}{$item.category_title}{else}Все{/if}</nobr></td>
							<td width="1%"><input type="checkbox" disabled="disabled" {if $item.is_show_main}checked="checked"{/if} title="Показывать на главной странице" /></td>
							<td width="1%"><input type="checkbox" disabled="disabled" {if $item.is_show}checked="checked"{/if} title="Показывать на сайте" /></td>
						</tr>
					{/foreach}
				</tbody>
			</table>
		</div>
	</div>
</div>
<form action="/{$admin_dir}/news/save/" method="post" class="page_form" enctype="multipart/form-data">
	<input type="hidden" name="id" value="{$id}" />
	<div class="widget fluid" id="settings" style="display:none;">
		<div class="formRow">
			<div class="grid3"><label>Заголовок страницы:</label></div>
			<div class="grid9"><input type="text" name="title_page" value="{$title_page}" /></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Заголовок для меню:</label></div>
			<div class="grid9"><input type="text" name="title" value="{$title}" /></div>
		</div>
		{$settings_block}
		<div class="formRow">
			<input type="submit" class="buttonS bLightBlue" value="Сохранить" />
			<a class="buttonS bRed" rel="{$id}" href="javascript:void(0);" style="margin-left: 10px;">
				<span class="icon-remove"></span><span>Удалить раздел</span>
			</a>
		</div>
	</div>
	<div class="widget fluid" id="seo" style="display:none;">
		<div class="formRow">
			<div class="grid3"><label>Описание страницы:</label></div>
			<div class="grid9"><input type="text" name="description" value="{$description}" /></div>
		</div>
		<div class="formRow">
			<div class="grid3"><label>Ключевые слова:</label></div>
			<div class="grid9"><input type="text" name="keywords" value="{$keywords}" /></div>
		</div>
		<div class="formRow">
			<input type="submit" class="buttonS bLightBlue" value="Сохранить" />
		</div>
	</div>
</form>
<div id="deleteVolumeModalDialog" style="display:none;" title="Удаление раздела">
	<p>Вы уверены что хотите удалить этот раздел?</p>
</div>
<div id="deleteModalDialog" style="display:none;" title="Удаление новости">
	<p>Вы уверены что хотите удалить эту новость?</p>
</div>