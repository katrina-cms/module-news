<div id="main">

    <!-- Текстовая страниа -->
    <div id="article">
        <div class="container  clearfix">
            
            <!-- Шапка новости -->
            <div class="title">
                <ul>
                    <li><a href="/">Lada Sport</a></li>
                    <li><a href="/news/{$pid}/">Новости</a></li>
                    {if $category_id}
                        <li><a href="/news/{$pid}/{$category_id}/">{$category_title}</a></li>
                    {/if}
                </ul>
                <h1>
                    <span>{$title}</span>
                    <div class="date">
                        <p>{$date}</p>
                    </div>
                </h1>
            </div>

            {if !empty($img_500x300) || !empty($note)}
				<div class="clearfix">
					{if $img_500x300}
						<div class="img">
							<img width="500" height="300" src="{$img_500x300}" alt="" />
							<!-- <p>
								Захватывающие заезды проводятся<br>
								на специально подготовленных автомобилях LADA Kalina
							</p> -->
						</div>
					{/if}
					{if $note}
						<div class="desc">
							<h2>{$note}</h2>
						</div>
					{/if}
				</div>
			{/if}
        </div>

        <!-- Текстовая область -->
        <div class="container content">
			{$text}
			{$photogallery_block}
		</div>
    </div>
</div>