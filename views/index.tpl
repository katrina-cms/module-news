<div id="main">
    <div id="news">
        <div id="news_list" data-url="/news/{$id}/{$cid}/" data-page="0" class="container  clearfix">
            <div class="title">
                <h1>{$title}</h1>
                <ul>
                    {foreach $categories_list as $cat}
						<li class="{$cat.active}">
							<a href="{$cat.link}"><span>{$cat.title}</span></a>
						</li>
					{/foreach}
                </ul>
            </div>
			{$news_list}
        </div>
    </div>
    <div id="preloader"></div>
</div>