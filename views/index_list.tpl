{foreach $news_list as $item}
	<div class="article clearfix {if $item.odd()}odd{/if}">
		{if $item.img_500x300}
			<div class="img">
				<a href="{$item.link}">
					<img src="{$item.img_500x300}" alt="" />
				</a>
			</div>
		{/if}
		<div class="date">
			<p>{$item.date}</p>
		</div>
		<div class="text">
			{if $item.note}
				<h2><a href="{$item.link}">{$item.title}</a></h2>
				<p>{$item.note}</p>
			{else}
				<h1><a href="{$item.link}">{$item.title}</a></h1>
			{/if}
		</div>
	</div>
{/foreach}
