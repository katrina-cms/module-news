<?php
	class news_adm_controller extends adm_controller {
		
		protected $module_id = 2;
		
		/**
		 * Общая страница раздела новости
		 */
		public function view($id) {
			
			// получаем данные по разделу
			$data = $this->news->getVolume($id);
			if (empty($data)) {
				$this->_url->referer();
			}
			
			self::$node_id	= (int)$id;
			self::$header	= $data['title'];
			$data['pid']	= (int)$data['id'];
			
			$formats = $this->_config->get('formats', 'news');
			
			// список новостей
			$data['news_list'] = $this->news->getNewsList($id);
			if (!empty($data['news_list'])) {
				foreach ($data['news_list'] as $i => &$item) {
					
					// форматирование даты
					$item['date'] = date('Y/m/d', strtotime($item['date']));
					
					// превью
					if (!empty($formats)) {
						foreach ($formats as $i => $f) {
							$img_path = $this->src_path.$f['folder'].DS.(int)$item['id'].'.jpg';
							if (file_exists($img_path)) {
								$item['img_'.$f['folder']] = '/modules/news/src/'.$f['folder'].'/'.(int)$item['id'].'.jpg';
							}
						}
					}
				}
			}
			
			// системные модули
			$data['settings_block'] = $this->all_controller->generateSettingsBlock($id, $this->module_id);
			
			$this->_tpl->render('news/view', $data, 'content');
		}
		
		/**
		 * Метод сохранения раздела новости
		 */
		public function save() {
			
			// проверяем наличие обязательных полей
			if (empty($_POST['id']) || empty($_POST['title'])) {
				$this->url->redirect('::referer');
			}
			
			$id = (int)$_POST['id'];
			
			// сохраняем параметры раздела новостей
			$main = array(
				'pid'         => (int)$_POST['pid'],
				'title'       => strip_tags($_POST['title']),
				'title_page'  => strip_tags($_POST['title_page']),
				'description' => strip_tags($_POST['description']),
				'keywords'    => strip_tags($_POST['keywords']),
				'inmenu'      => (int)$_POST['inmenu']
			);
			$this->_db->update('main', $main, $id);
			
			// особая ситуация: изменение основного модуля
			if ((int)$_POST['module_id'] !== $this->module_id) {
				$this->modules_controller->changeModule($id, $this->module_id, (int)$_POST['module_id'], $_POST['link']);
			}
			
			$this->_session->set('alert', ALERT_CHANGE_DATA);
			$this->_url->redirect($this->main->buildAdminURL($id));
		}
		
		/**
		 * Страница добавления новой новости
		 */
		public function add($pid = 0) {
			$this->item($pid);
		}
		
		/**
		 * Страница добавления и редактирования новой новости
		 */
		public function item($pid = 0, $id = 0) {
			
			// получаем данные по разделу новости
			$category = $this->news->getVolume($pid);
			if (empty($category)) {
				$this->_url->referer();
			}
			
			// получаем данные по новости
			$data = $this->news->getItem($id);
			
			if (empty($data)) {
				self::$header					= 'Новая новость';
				$data['id']						= 0;
				$data['date']					= date('d.m.Y');
				$data['category_id']			= 0;
				$data['is_show_checked']		= 'checked';
				$data['is_show_main_checked']	= 'checked';
			} else {
				self::$header					= $data['title'];
				self::$node_id					= $data['id'];
				$data['date']					= date('d.m.Y', strtotime($data['date']));
				$data['category_id']			= (int)$data['category_id'];
				$data['is_show_checked']		= (!empty($data['is_show']))		? 'checked' : '';
				$data['is_show_main_checked']	= (!empty($data['is_show_main']))	? 'checked' : '';
				
				// превью
				$formats = $this->_config->get('formats', 'news');
				if (!empty($formats)) {
					foreach ($formats as $i => $f) {
						$img_path = $this->src_path.$f['folder'].DS.(int)$data['id'].'.jpg';
						if (file_exists($img_path)) {
							$data['img_'.$f['folder']] = '/modules/news/src/'.$f['folder'].'/'.(int)$data['id'].'.jpg';
						}
					}
				}
				
				// Подключаем модуль фотогалереи (слайдер)
				$data['photogallery_block'] = $this->photogallery_controller->block($id, $this->module_id);
				
			}
			
			// подключаем скписок категорий
			$data['categories_list'] = $this->news->getAllCategories($pid);
			
			// конфиг
			if (!empty($data['config'])) {
				$data['config'] = unserialize($data['config']);
				foreach ($data['config'] as $key => $value) {
					$data['config_'.$key] = $value;
				}
				unset($data['config']);
			}
			
			$data['pid']	= (int)$category['id'];
			self::$node_id	= (int)$category['id'];
			self::breadcrumb($data['pid'], self::$header, '');
			
			
			// контент
			if (file_exists($this->src_path.'text'.DS.$data['id'].'.txt')) {
				$data['text'] = file_get_contents($this->src_path.'text'.DS.$data['id'].'.txt');
			}
			
			// основной шаблон
			$this->_tpl->render('news/item', $data, 'content');
		}
		
		/**
		 * Метод сохранения новости
		 */
		public function item_save() {
			
			$data = array(
				'pid'			=> (int)$_POST['pid'],
				'category_id'	=> (int)$_POST['category_id'],
				'title'			=> strip_tags($_POST['title']),
				'note'			=> strip_tags($_POST['note']),
				'date'			=> date('Y-m-d', strtotime($_POST['date'])),
				'description'	=> strip_tags($_POST['description']),
				'keywords'		=> strip_tags($_POST['keywords']),
				'is_show'		=> (int)(!empty($_POST['is_show'])),
				'is_show_main'	=> (int)(!empty($_POST['is_show_main'])),
				'config'		=> serialize($_POST['config'])
			);
			
			if (empty($_POST['id'])) {
				$id = (int)$this->_db->insert('news', $data);
			} else {
				$id = (int)$_POST['id'];
				$this->_db->update('news', $data, $id);
			}
			
			// сохранение коннента
			if (isset($_POST['text'])) {
				$text = $_POST['text'];
				$this->_file->toFile($this->src_path.'text'.DS.$id.'.txt', $text);
			}
			
			// сохранение изображения
			if (isset($_FILES['preview']['tmp_name']) && file_exists($_FILES['preview']['tmp_name'])) {
				$formats = $this->_config->get('formats', 'news');
				if (!empty($formats) && $this->_image->analyze($_FILES['preview']['tmp_name'])) {
					foreach ($formats as $i => $f) {
						$this->_image->toFile($this->src_path.$f['folder'].DS.$id.'.jpg', $f['q'], $f['w'], $f['h']);
					}
				}
			}
			
			$this->_session->set('alert', ALERT_CHANGE_DATA);
			// $this->_url->redirect('/'.$this->admin_dir.'/news/view/'.(int)$_POST['pid'].'/');
			$this->_url->redirect('/'.$this->admin_dir.'/news/item/'.(int)$_POST['pid'].'/'.(int)$id.'/');
		}
		
		public function deleteImg($id) {
			$pid = $this->db->get_one('SELECT pid FROM news WHERE id = '.(int)$id);
			if (empty($pid)) {
				$this->url->redirect('::referer');
			}
			
			//Проверяем права
			if (!$this->role_controller->CheckAccess(2, $pid)) $this->role_controller->AccessError();
			
			if (file_exists($this->path.$id.'.jpg')) unlink($this->path.$id.'.jpg');
			
			$this->session->set('alert', ALERT_DEL_IMAGE);
			$this->url->redirect('::referer');
		}
		
		
		// удаление одной записи
		public function delete_item($id) {
			
			$sql  = 'SELECT id, pid '.
					'FROM news '.
					'WHERE id = '.(int)$id;
			$data = $this->_db->get_row($sql);
			if (empty($data) || empty($data['pid'])) {
				$this->_url->redirect('::referer');
			}
			
			// Получаем корректные форматы изображений фотогалереи (для построения путей)
			$formats = $this->_config->get('formats', 'photogallery');
			
			// Получаем список изображений фотогалереи удаляемой новости и удаляем файлы на сервере и записи об изображениях в БД
			$sql  = 'SELECT id '.
					'FROM photogallery_images '.
					'WHERE module_id = '.(int)$this->module_id.' AND pid = '.(int)$id;
			$photos_list = $this->_db->get_all_one($sql);
			if (!empty($photos_list)) {
				foreach ($photos_list as $i => $photo) {
					if (!empty($formats)) {
						foreach ($formats as $j => $format) {
							$path = MODULES.'photogallery'.DS.'src'.DS.$format['folder'].DS.(int)$photo.'.jpg';
							if (file_exists($path)) {
								unlink($path);
							}
						}
					}
					$this->_db->delete('photogallery_images', (int)$photo);
				}
			}
			
			// Удаляем запись о данной новости из БД
			$this->_db->delete('news', (int)$data['id']);
			
			$this->_session->set('alert', 'Новость была удалена');
			$this->_url->redirect('/admin/news/view/'.$data['pid'].'/');
		}
		
		
		// -- групповое удаление
		public function group_delete() {
			if (!empty($_POST['ids'])) {
				foreach ($_POST['ids'] as $id) {
					$this->delete($id);
				}
			}
			$this->url->redirect('::referer');
		}
		
		private function delete($id) {
			$pid = $this->db->get_one('SELECT pid FROM news WHERE id = '.(int)$id);
			if (empty($pid)) {
				$this->url->redirect('::referer');
			}
			
			//Проверяем права
			if (!$this->role_controller->CheckAccess(2, $pid)) 
				$this->role_controller->AccessError();
			
			$this->db->delete('news', (int)$id);
			
			if (file_exists($this->path.$id.'.txt')) unlink($this->path.$id.'.txt');
			if (file_exists($this->path.$id.'.jpg')) unlink($this->path.$id.'.jpg');
			
			$this->trash_controller->delete_addition($id, $this->module_id, true);
		}		
	}
?>