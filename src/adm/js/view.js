$(document).ready(function() {
	
	$('#deleteModalDialog').dialog({
		autoOpen: false,
		modal: true,
		resizable: false,
		buttons: {
			'Удалить': function() {
				var button	= $($(this).data('button'));
				var id		= button.attr('rel');
				
				location.href = '/admin/news/delete_item/'+id+'/';
			},
			'Отмена': function() {
				$(this).dialog('close');
			}
		}
	});
	$('.dTable .deleteBtn').live('click', function() {
		$('#deleteModalDialog').data('button', this);
		$('#deleteModalDialog').dialog('open');
		return false;
	});
	
	$('#deleteVolumeModalDialog').dialog({	
		autoOpen: false,
		modal: true,
		resizable: false,
		buttons: {
			'Удалить': function() {
				var button	= $($(this).data('button'));
				var id		= button.attr('rel');
				location.href = '/admin/module/delete/'+id+'/';
			},
			'Отмена': function() {
				$(this).dialog('close');
			}
		}
	});
	$('#settings .bRed').live('click', function() {
		$('#deleteVolumeModalDialog').data('button', this);
		$('#deleteVolumeModalDialog').dialog('open');
		return false;
	});
	
});