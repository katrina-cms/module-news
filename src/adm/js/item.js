$(document).ready(function() {
	
	$('#roundedPreview').click(function() {
		
		var pos = $('#imagePosField').val() || 'left';
		$('#popupRoundedPreview .imageView[data-pos='+pos+']').click();
		
		$('#popupRoundedPreview').dialog({
			width: 640,
			modal: true,
			resizable: false,
			buttons: {
				'Сохранить': function() {
					var active = $('#popupRoundedPreview .active');
					if(active.length) {
						$('#imagePosField').val(active.data('pos'));
					}
					$('#roundedPreview').attr('class', 'pos_'+active.data('pos'));
					$(this).dialog('close');
				},
				'Отмена': function() {
					$(this).dialog('close');
				}
			}
		});
		
	});
	
	$('#popupRoundedPreview .imageView').click(function() {
		if($(this).hasClass('active')) {
			return false;
		}
		
		$('#popupRoundedPreview .active').removeClass('active');
		$(this).addClass('active');
			
		
		return false;
	});
	
	
});