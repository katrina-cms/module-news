$(function() {

	function viewFooter() {
		if($('#news_list').data('loading')) {
			return false;
		}
		
		var page	= $('#news_list').data('page') || 0;
		var url		= $('#news_list').data('url');
		
		$.getJSON(url+(page + 1)+'/', function(r) {
			if(r.status == 'ok' && r.data.news_list) {
				
				$('#news_list').append(
					'<div class="page-'+(page + 1)+'" style="display:none;">'+
						r.data.news_list+
					'</div>'
				);
				
				var count = $('#news_list .page-'+(page + 1)+' .img img').length;
				var width = 100 / (count + 1);
				$('#news_list .page-'+(page + 1)+' .img img').load(function() {
					var step	= $('#preloader').data('step') + 1;
					var parts	= $('#preloader').data('parts');
					
					var w = (step * width > 100) ? 100 : step * width;
					$('#preloader').stop().animate({width: w+'%'}, 200, function() {
						var step	= $(this).data('step') + 1;
						var parts	= $(this).data('parts');
						if(step >= parts) {
							$(this).fadeOut(function() {
								$('#news_list .page-'+(page + 1)).show();
								$('#news_list').data('page', page + 1);
								$('#news_list').data('loading', false);
							});
						}
					});
					$('#preloader').data('step', step);
				});
				
				$('#preloader').data('step', 1).data('parts', count + 1).css('width', '0').show();
				$('#preloader').animate({width: width+'%'}, 200);
			}
		});
		
		$('#news_list').data('loading', true);
	}

	$(window).scroll(function() {
		var offset_pos = $(document).scrollTop() + $(window).height();
		var footer_top = $('#footer').position().top;
		if(offset_pos >= footer_top) {
			viewFooter();
		}
	});

});