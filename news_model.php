<?php
	class news_model extends core_model {
			
		/**
		 * Список новостей для админки
		 */
		public function getNewsList($pid) {
			$sql  = 'SELECT n.id, n.pid, n.title, n.date, n.is_show, n.is_show_main, '.
						'n.category_id, nc.title category_title '.
					'FROM news n '.
						'LEFT JOIN news_categories nc ON n.category_id = nc.id '.
					'WHERE n.pid = '.(int)$pid.' '.
					'ORDER BY n.date DESC, n.id DESC';
			return $this->_db->get_all($sql);
		}
		
		public function getList($pid, $cid, $page, $count) {
			$sql  = 'SELECT * '.
					'FROM news '.
					'WHERE pid = '.(int)$pid.' AND is_show = 1 '.
						(!empty($cid) ? 'AND category_id = '.(int)$cid.' ' : '').
					'ORDER BY date DESC, id DESC '.
					'LIMIT '.((int)$page * (int)$count).', '.(int)$count;
			return $this->_db->get_all($sql);
		}
		
		public function getItem($id) {
			$sql  = 'SELECT n.*, nc.title category_title '.
					'FROM news n '.
						'LEFT JOIN news_categories nc ON n.category_id = nc.id '.
					'WHERE n.id = '.(int)$id;
			return $this->_db->get_row($sql);
		}
		
		public function getItemActive($id) {
			$sql  = 'SELECT n.*, nc.title category_title '.
					'FROM news n '.
						'LEFT JOIN news_categories nc ON n.category_id = nc.id '.
					'WHERE n.id = '.(int)$id.' AND n.is_show = 1';
			return $this->_db->get_row($sql);
		}
		
		public function issetVolume($id) {
			$sql  = 'SELECT 1 FROM main '.
					'WHERE id = '.(int)$id.' AND module_id = 2';
			return (bool)$this->_db->get_one($sql);
		}
		
		public function getVolume($id) {
			$sql  = 'SELECT id, title, title_page, keywords, description '.
					'FROM main '.
					'WHERE id = '.(int)$id.' AND module_id = 2';
			return $this->_db->get_row($sql);
		}

		/**
		 * Получаем список только тех категорий,
		 * по которым есть связанные новости
		 */
		public function getCategories($pid) {
			$sql  = 'SELECT nc.id, nc.title '.
					'FROM news n '.
						'LEFT JOIN news_categories nc ON n.category_id = nc.id '.
					'WHERE n.pid = '.(int)$pid.' AND n.category_id > 0 '.
					'GROUP BY n.category_id '.
					'ORDER BY nc.sort, nc.id';
			return $this->_db->get_all($sql);
		}
		
		/**
		 * Получаем список все категорий
		 */
		public function getAllCategories($pid) {
			$sql  = 'SELECT nc.id, nc.title '.
					'FROM news_categories nc '.
					'WHERE nc.pid = '.(int)$pid.' '.
					'ORDER BY nc.sort, nc.id';
			return $this->_db->get_all($sql);
		}
		
	}
?>